<?php

    function hao_setup(){
        
        
        // add nav
        register_nav_menu('primary', __('Primary Menu','hao'));
    }

     add_action('after_setup_theme','hao_setup');
     
     
     function hao_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// add website name
	$title .= get_bloginfo( 'name' );

        // add description for home page
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

        // add page
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'hao' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'hao_wp_title', 10, 2 );

/*
     
 *     search form
 *  */

function hao_search_form( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="input-append pull-right" id="search"><label class="hide screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input class="span3" type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input class="btn" type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
    </div>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'hao_search_form' );


/*
 * Bootstrap
 */

class hao_Nav_Walker extends Walker_Nav_Menu {
 
 
function start_lvl( &$output, $depth ) {
 
$indent = str_repeat( "\t", $depth );
$submenu = ($depth > 0) ? ' sub-menu' : '';
$output	.= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
 
}
 
function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
 
 
$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
$li_attributes = '';
$class_names = $value = '';
 
$classes = empty( $item->classes ) ? array() : (array) $item->classes;
// managing divider: add divider class to an element to get a divider before it.
$divider_class_position = array_search('divider', $classes);
if($divider_class_position !== false){
$output .= "<li class=\"divider\"></li>\n";
unset($classes[$divider_class_position]);
}
$classes[] = ($args->has_children) ? 'dropdown' : '';
$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
$classes[] = 'menu-item-' . $item->ID;
if($depth && $args->has_children){
$classes[] = 'dropdown-submenu';
}
 
 
$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
$class_names = ' class="' . esc_attr( $class_names ) . '"';
 
$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
 
$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
$attributes .= ($args->has_children) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
 
$item_output = $args->before;
$item_output .= '<a'. $attributes .'>';
$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
$item_output .= ($depth == 0 && $args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
$item_output .= $args->after;
 
 
$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
 
function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
//v($element);
if ( !$element )
return;
 
$id_field = $this->db_fields['id'];
 
//display this element
if ( is_array( $args[0] ) )
$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
else if ( is_object( $args[0] ) )
$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'start_el'), $cb_args);
 
$id = $element->$id_field;
 
// descend only when the depth is right and there are childrens for this element
if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
 
foreach( $children_elements[ $id ] as $child ){
 
if ( !isset($newlevel) ) {
$newlevel = true;
//start the child delimiter
$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
}
$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
}
unset( $children_elements[ $id ] );
}
 
if ( isset($newlevel) && $newlevel ){
//end the child delimiter
$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
}
 
//end this element
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'end_el'), $cb_args);
 
}
 
}



function hao_avatar_css($class) {
	$class = str_replace("class='avatar", "class='img-circle", $class) ;
return $class;
}
add_filter('get_avatar','hao_avatar_css');


// widget


function hao_widgets_init() {
  register_sidebar( array(
    'name'          =>  __( 'Sidebar Bottom', 'hao' ),
    'id'            =>  'sidebar-bottom',
    'description'   =>  __( 'Sidebar Bottom', 'hao' ),
    'before_widget' =>  '<div id="%1$s" class="%2$s"><section>',
    'after_widget'  =>  '</section></div>',
    'before_title'  =>  '<h3><span>',
    'after_title'   =>  '</span></h3>',
  ) );
  register_sidebar( array(
    'name'          =>  __( 'Sidebar Right', 'hao' ),
    'id'            =>  'sidebar-right',
    'description'   =>  __( 'Sidebar Right', 'hao' ),
    'before_widget' =>  '<div id="%1$s" class="%2$s"><section>',
    'after_widget'  =>  '</section></div>',
    'before_title'  =>  '<div class="title-line"><h3>',
    'after_title'   =>  '</h3></div>',
  ) );
  
}
add_action( 'widgets_init', 'hao_widgets_init' );


function hao_the_meta() {
   if ( $keys = get_post_custom_keys() ) {
      echo "<dl class='dl-horizontal'>\n";
      foreach ( (array) $keys as $key ) {
              $keyt = trim($key);
                if ( is_protected_meta( $keyt, 'post' ) )
                      continue;
              $values = array_map('trim', get_post_custom_values($key));
              $value = implode($values,', ');
              echo apply_filters('the_meta_key', "<dt>$key</dt><dd>$value</dd>\n", $key, $value);
      }
      echo "</dl>\n";
    }
}


/*
 * comment
 */
if ( ! function_exists( 'hao_comment' ) ) :
function hao_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'hao' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'fenikso' ), '<span class="edit-link">', '</span>' ); ?>
		</p>
	<?php
		break;
		default :
		
		global $post;
	 ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="media comment">
			<div class="pull-left">
  			<?php 
  			  echo get_avatar( $comment, 64 ); 
  			?>
			</div>
			<?php 
			  if ( '0' == $comment->comment_approved ) : ?>
  			<p class="comment-awaiting-moderation">
  			  <?php _e( 'Your comment is awaiting moderation.', 'hao' ); ?>
  			</p>
			<?php endif; ?>
			<div class="media-body">
				<h4 class="media-heading">
  				<?php 
  				    printf( '%1$s %2$s',
  						get_comment_author_link(),
  					
  						( $comment->user_id === $post->post_author ) ? '<span class="label label-info"> ' . __( 'Post author', 'fenikso' ) . '</span>' : ''
  					);
  				?>
  		    <small>
    				<?php 
    				    printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
    						esc_url( get_comment_link( $comment->comment_ID ) ),
    						get_comment_time( 'c' ),
    					
    						sprintf( __( '%1$s %2$s', 'hao' ), get_comment_date(), get_comment_time() )
    					);
    				?>
  				</small>
				</h4>
				<?php 
				  comment_text(); 
				?>
				<?php 
				  edit_comment_link( __( 'Edit', 'hao' ), '<p class="edit-link">', '</p>' ); 
				?>
				<div class="reply">
					<?php 
					  comment_reply_link( array_merge( $args, array( 
					    'reply_text' =>  __( 'Reply', 'hao' ), 
					    'after'      =>  ' <span>&darr;</span>', 
					    'depth'      =>  $depth, 
					    'max_depth'  =>  $args['max_depth'] ) ) ); 
					?>
				</div>
			</div>
		</article>
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

function hao_scripts_styles() {
	global $wp_styles;

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'hao_scripts_styles' );

?>
