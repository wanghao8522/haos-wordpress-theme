    <footer id="footer">
      <div class="container">
          &copy; <?php echo date('Y')?> &nbsp;<?php bloginfo('name');?>
      </div>
    </footer><!-- end #footer -->
    
    <?php wp_footer();?>
    </body>
    </html>