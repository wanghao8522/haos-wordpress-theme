<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta <?php bloginfo('charset');?>>

<title><?php wp_title('&hearts;',true,'right');?></title>

<?php wp_head();?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/style.css"/>

<script src="<?php echo get_template_directory_uri()?>/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/hao.js"></script>

</head>

<body>

	<header>
	 
	 <div class="container">
	 	<div class="row">
	 	
	 	  <div class="span6" >
	 	  
	 	  <a href="<?php echo esc_url( home_url('/'));?>" title="<?php esc_attr_e('Jump to the home page','hao')?>">
                      <img id="logo" src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="<?php esc_attr(get_bloginfo('name','display'))?>"/>
	 	  
	 	  </a>
	 	 
	 	  
	 	  </div>
	 	  
	 	  <div class="span6">
                      
                      <?php get_search_form();?>
	
               
            </div>  
	 	  
	 	  </div>
	 	
	 	</div>
	 
	 
	 </div>
	
	
	</header>
       
    
    <nav id="navigation">

           <div class="container">
                       
      <div class="navbar">
      
          <?php wp_nav_menu(
                  
                  array(
                      
                       'theme_location' => 'primary',
                      
                       'menu_class'    => 'nav',
                      
                        'container'    =>false,
                      
                        'walker'       =>new hao_Nav_Walker,
                      
                  )
          
                  
                  );?>
      </div>
    </div>

     </nav>