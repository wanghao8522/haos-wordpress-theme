<?php get_header();  ?>
<div id="main-content">
  <div class="container">
    <?php while ( have_posts() ) : the_post();  ?>
    <div class="row">
       <div class="span8">      
         <div id="post-<?php the_ID(); ?>">
          <?php
        
          $attachments = array_values( get_children( array( 
              'post_parent'     => $post->post_parent, 
              'post_status'     => 'inherit', 
              'post_type'       => 'attachment', 
              'post_mime_type'  => 'image', 
              'order'           => 'ASC', 
              'orderby'         => 'menu_order ID' 
              ) ) );
          foreach ( $attachments as $k => $attachment ) :
               if ( $attachment->ID == $post->ID )
                  break;
          endforeach;
          $k++;
       
          if ( count( $attachments ) > 1 ) :
             if ( isset( $attachments[ $k ] ) ) :
              
                $next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
             else :
             
                $next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
             endif;
          else :
         
               $next_attachment_url = wp_get_attachment_url();
          endif;
          ?>
          <a href="<?php echo esc_url( $next_attachment_url ); ?>" 
             title="<?php the_title_attribute(); ?>">
            <?php 
            $attachment_size = apply_filters( 'fenikso_attachment_size', array( 960, 960 ) );
            echo wp_get_attachment_image( $post->ID, $attachment_size );
            ?>
          </a>
          <?php if ( ! empty( $post->post_excerpt ) ) : ?>
          <div class="content-box">
            <?php the_excerpt();  ?>
          </div>
          <?php endif; ?>                  
         </div>
         <?php if ( function_exists( 'bcn_display' ) ):  ?>    
         <ul class="breadcrumb">
            <?php bcn_display(); ?>
         </ul>
         <i class="icon-bow"></i>
         <?php endif; ?>    
       <?php comments_template();  ?> 
       </div>
       <div class="span4">       
         <h1>
           <?php the_title();  ?>
         </h1>
         <?php the_content();  ?>
         <footer>
          <?php
             $metadata = wp_get_attachment_metadata();   
             printf( __( '<dl><dt>Published</dt> <dd><time class="entry-date" datetime="%1$s">%2$s</time></dd> <dt>Image Size</dt><dd><a href="%3$s" title="Link to full-size image">%4$s &times; %5$s</a></dd><dt>Belong</dt><dd><a href="%6$s" title="Return to %7$s" rel="gallery">%8$s</a></dd></dl>', 'fenikso' ),
                  esc_attr( get_the_date( 'c' ) ),
                  esc_html( get_the_date() ),
                  esc_url( wp_get_attachment_url() ),
                  $metadata['width'],
                  $metadata['height'],
                  esc_url( get_permalink( $post->post_parent ) ),
                  esc_attr( strip_tags( get_the_title( $post->post_parent ) ) ),
                  get_the_title( $post->post_parent )
             );
          ?>
           <a class="btn btn-mini" href="<?php echo get_edit_post_link( $page->ID); ?>"> 
             <i class="icon-wrench"></i>
           </a>
         </footer>
         <ul class="pager">
           <li class="previous">
             <?php previous_image_link( false, __( '&larr; Previous', 'fenikso' ) ); ?>
           </li>
           <li class="next">
             <?php next_image_link( false, __( 'Next &rarr;', 'fenikso' ) ); ?>
           </li>
         </ul>
       </div>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php get_sidebar(); ?>
<?php get_footer();  ?> 