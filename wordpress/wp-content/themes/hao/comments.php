<section id="comments"> 
    
    <h2>
        
        <?php printf( 
            _n( '&ldquo; %2$s &rdquo; comment %1$s', 
                '&ldquo; %2$s &rdquo; comments %1$s', 
                 'fenikso'
               ),
              '<span class="badge badge-important">' . get_comments_number() . '</span>',  
                get_the_title()
               );
        ?>
        
    </h2>
    
    <ol>
        
         <?php wp_list_comments(
            
            array(
          
                'callback'    =>'hao_comment',
            )
         
            );?>
        
        
    </ol>
    
   
    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  ?>
  <ul class="pager">
    <li class="previous">
      <?php previous_comments_link( __( '&larr; Older Comments', 'hao' ) ); ?>
    </li>
    <li class="next">
       <?php next_comments_link( __( 'Newer Comments &rarr;', 'hao' ) ); ?>
    </li>
  </ul>
<?php endif; ?>
    <?php
    $comment_form_args = array(

  	'comment_field'         => '<label for="comment" class="control-label">' . 
          	                    _x( 'Comment', 'noun' ) . 
          	                   '</label>
          	                    <textarea id="comment" name="comment" cols="45" rows="5" class="span8" aria-required="true">
          	                    </textarea>',
  
  	'comment_notes_before'  => ' ',
  
  	'comment_notes_after'   => ' ',

  	'fields'                => apply_filters( 'comment_form_default_fields', array(
 
		'author'                => '<label for="author" class="control-label">' . 
                		            __( 'Name', 'hao' ) . 
                		            '</label> ' . 
                		            '<div class="controls">' . 
                		            '<input id="author" name="author" type="text" value="' . 
                		             esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' . 
                		             ( $req ? '<span class="required help-inline">*</span>' : '' ) . 
                		            '</div>',
        		            
		'email'                 => '<label for="email" class="control-label">' . 
                		            __( 'Email', 'hao' ) . 
                		            '</label> ' . 
                		            '<div class="controls">' . 
                		            '<input id="email" name="email" type="text" value="' . 
                		             esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' . 
                		             ( $req ? '<span class="required help-inline">*</span>' : '' ) . 
                		             '</div>',
           		             
  	'url'                   => '<label for="url" class="control-label">' . 
                  		          __( 'Website', 'hao' ) . 
                  		          '</label>' .  
                  		          '<div class="controls">' . 
                  		          '<input id="url" name="url" type="text" value="' . 
                  		           esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>' ) 
) );
    
    
    ?>
    <?php comment_form();?>
    
</section>